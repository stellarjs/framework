module.exports = {
  __useCache: false,
  __cachePath: 'cache',
  __viewLocals: {
      testViewLocal: 'testViewLocal',
  },
  __assetsFolder: {
      virtualURL: 'media',
      publicDir: 'public'
  },
  __port: 4000,
  __domain: 'localhost',
  __session: {
      secret: 'MySuperSecret',
      secure: true
  },
  __db_connections : [
      {
          name     : 'mainConnection',
          host     : 'localhost',
          user     : 'root',
          password : 'root',
          database : 'stellar_test_main',
          isDefault: true
      },
      {
          name     : 'secondConnection',
          host     : 'localhost',
          user     : 'root',
          password : 'root',
          database : 'stellar_test_second'
      }
  ]

};
