"use strict";

var Controller = require(Stellar.Config.moduleRootPath + 'controllers/controller');

class HomeController extends Controller {

    constructor() {
        // Mandatory to call super();
        super();
    }

    homeAction(req, res, next) {
        res.send('Home action, it works!');
    }
}

module.exports = HomeController;
