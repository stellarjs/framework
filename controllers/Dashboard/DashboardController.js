"use strict";

/* Mandatory variable for a Controller to be extended by it*/
var Controller = require(Stellar.Config.moduleRootPath + 'controllers/controller');
/* Models */
var UserModel = require(Stellar.Config.modelsPath + 'User');

class DashboardController extends Controller {

    constructor() {
        // Mandatory to call super()
        super();
    }

    /*
     *    Whenever you are using calls to objects model you have to specify async
     *    keyword before method declaration in order to have synchronous DB calls
     */
    async dashboardAction(req, res, next) {
        // Create new user (Checked: OK)
        /*var u_user = new UserModel();
        u_user.name = 'testName_' + Date.now();
        u_user.lastname = 'testLastName_' + Date.now();
        u_user.email = 'testEmail_' + Date.now();
        u_user.save();*/

        var userModel = new UserModel();
        var userSpecificFind = await userModel.find(2);

        // Get All Users (Checked: OK)
        var allUsers = await userModel.all();



        res.render('dashboard', {
            users: allUsers,
            userId2: userSpecificFind
        });
    }
}

module.exports = DashboardController;
