"use strict"

class Auth {

    // Default fallback method
    process(req, res, next) {
        console.log('Auth default process() method called');
        return true;
    }

    // Custom method
    authenticate(req, res, next) {
        console.log('Auth custom authenticate() method called');
        //res.status(403).send('Forbidden');
        next();
    }
}

module.exports = Auth;
