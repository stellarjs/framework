"use strict";

//@TODO: Handle prefiexed routes

Stellar.Kernel.routesCollection.addRoute({
    // Required
    name: 'dashboard',
    url: '/dashboard',
    httpMethods: ['GET', 'POST'],
    controller: 'Dashboard/DashboardController@dashboardAction',
    // Optionnal
    middlewares: [
        'Auth/Auth@authenticate'
    ]
});



/*Stellar.Kernel.routesCollection.addRoutes([
    {
        // Required
        name: 'home',
        url: '/',
        httpMethods: ['GET', 'POST'],
        controller: 'HomeController@homeAction',
    },
    {
        // Required
        name: 'homeblog',
        url: '/homeblog',
        httpMethods: ['GET', 'POST'],
        controller: 'HomeController@blogAction',
        // Optionnal
        middlewares: [
            'Auth/Auth@authenticate'
        ]
    }
])*/
