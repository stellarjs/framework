"use strict";

//@TODO: Handle prefiexed routes

Stellar.Kernel.routesCollection.addRoute({
    // Required
    name: 'home',
    url: '/',
    httpMethods: ['GET'],
    controller: 'HomeController@homeAction'
});
