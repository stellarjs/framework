"use strict";

var Model = require(Stellar.Config.moduleRootPath +  'models/model');

class User extends Model {

    settings() {
        return {
            table: 'user',
            primaryKey: 'id',
            timestamp: false,
            guarded: [],
            fillable: [],
            connectionName: null,
            path: ''
        };
    }

    constructor() {
        super();
    }

}

module.exports = User;
